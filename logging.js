"use strict";

startLog("PARSER: Inline Rolls");
theHTML = addInlineRolls(theHTML);
endLog();

startLog("PARSER: Entity Links");
theHTML = addEntityLinks(theHTML);
endLog();


//Helps with logging
function startLog(label) {
   console.groupCollapsed(label);
   console.time();
}

function endLog() {
   console.timeEnd();
   console.groupEnd();
}

//PASS #8: Create Journal Links inside content
// regex = /<(?<hlevel>h[1-4]) id="(?<headingID>\w+)">(?<description>[0-9]{1,2}[A-Z, ]*\.\s[\w\s']+)<\/\k<hlevel>/g

/*
BGDiA: h4, LetterNumber. (E13.)
HotDQ: h3, Number.Letters (2. V, W)
CoS: h3, Letter. (X.) & LetterNumber. (E13.)
*/

(async () => {
   //const theJournals = game.journal.filter(j => j.getFlag("world","ddbhp").status === "init");

   const journal = game.journal.getName("Chapter 7: Hunting Lodge");
   const chapterFolder = journal.folder;
   const chapterBaseUrl = journal.getFlag("world", "ddbhp").originalUrl;

   const html = journal?.data.content;
   if (!html)
      return;
   /*
   TODO: Need to have the option to select heading level here, what headings for folders, and what heading for sections/areas
   */

   // Create Section JEs
   let sections = html.matchAll(
      /<h3 id="(?<headingID>\w+)">(?<sectionName>[0-9]{1,2}[A-Z, ]*\.\s[\w\s']+)<\/h3>(?<sectionContent>.*?)(?=<h[123])/gs,
   );

   // Parse each sections
   for (const s of sections) {
      let sectionHeaderId = s[1];
      let sectionName = s[2];
      let sectionContent = s[3];

      const sectionJournal = await JournalEntry.create(
         {
            name: sectionName,
            content: sectionContent,
            folder: chapterFolder.id,
            "flags.world.ddbhp": { status: "parsed", originalUrl: `${chapterBaseUrl}#${sectionHeaderId}` },
         },
         { renderSheet: false },
      );
   }

})();
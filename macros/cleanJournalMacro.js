(async () => {
   const theJournals = game.journal.filter(j => j.getFlag("world","ddbhp").status === "init");
   let options = "";
   theJournals.forEach(j => options += `<option value="${j.id}">${j.name}</option>`)
   const content = `<label for="journal">Choose a journal:</label>
   <select name="journal" id="journal">
   ${options}
   </select>`;

   let d = new Dialog({
      title: "Parse Journal",
      content: content,
      buttons: {
         ok: {
            icon: '<i class="fas fa-check"></i>',
            label: "Ok",
            callback: (html) => {
               cleanJournal(html.find('#journal').val());
            },
         },
         cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: "Cancel",
            callback: () => console.log("No journal parsed"),
         },
      },
      default: "cancel",
   });
   d.render(true);

   async function cleanJournal(journalId) {
      const journal = game.journal.get(journalId);
      const html = journal?.data.content;
      if (!html)
         return;
      
      const cleanedHTML = removeStyles(cleanupHTML(html));

      await journal.update({content: cleanedHTML, flags: {"ddbji": {status: "cleaned"}}});
   }


   //Remove some crap
   function cleanupHTML(html) {
      return html.replace(
         /( data-.*?=".*?"| class=".*?"| title=".*?"|<select.*?>.*?<\/select>)/gms,
         ``
      );
   }

   //Replace SPAN containing font-* attributes with STRONG and EM tags, and then removes all style attributes
   function removeStyles(html) {
      const regex_SpanStyleCleanup = /<span style=".*?(font-weight: bold;)?.*?(font-style: italic;)?.*?">(?<content>.*?)<\/span>/g;
      const regex_RemoveStyles = / style=".*?"/g;
      const regex_htmlEntities = /(&nbsp;|&rsquo;)/g;

      return html
         .replace(
            regex_SpanStyleCleanup,
            function (wholeMatch, hasBold, hasItalics, content) {
               const isBold = wholeMatch.includes(`font-weight: bold;`);
               const isItalics = wholeMatch.includes(`font-style: italic;`);

               let result = ``;
               result += isBold ? "<strong>" : "";
               result += isItalics ? "<em>" : "";
               result += content;
               result += isBold ? "</strong>" : "";
               result += isItalics ? "</em>" : "";

               return result;
            }
         )
         .replace(regex_RemoveStyles, ``)
         .replace(regex_htmlEntities, (match) =>
            match === "&nbsp;" ? " " : "'"
         );
   }
})();

//Macro to Initialize

(async () => {
   const jefolder = await Folder.create({
      name: "DDB Adventure book",
      type: "JournalEntry",
      parent: null,
      sorting: "m",
      "flags.world.ddbhp-folder": true,
   });

   const tocJE = await JournalEntry.create(
      {
         name: "Table of Contents",
         content: "",
         folder: jefolder.id,
         "flags.world.ddbhp": {status: "init"},
      },
      { renderSheet: true }
   );

   ui.notifications.notify(
      "Created the DDB Journal Import folder and instructions."
   );
})();

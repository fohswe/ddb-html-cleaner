(async () => {
   const ddbJiToc = game.journal.find(
      j =>
         j.getFlag("world", "ddbhp")?.status === "init" &&
         j.name === "Table of Contents",
   );
   const ddbJiFolder = game.folders.find(
      f => f.getFlag("world", "ddbhp-folder") && f.name == "DDB Adventure book",
   );

   if (ddbJiToc === null || ddbJiFolder === null) {
      ui.notifications.warn("Couldn't find any content to parse!");
      return; //Couldn't find anything to parse
   }

   if (ddbJiToc.data.content === null) {
      ui.notifications.warn(
         "The Table of Contents journal seems to be empty. Please paste in the Table of Contents from DDB first.",
      );
      return;
   }

   //Sanitize HTML
   const tocHTML = removeStyles(cleanupHTML(ddbJiToc.data.content));

   //Find base url for this adventure
   let findBaseUrl = tocHTML.match(
      /https:\/\/www\.dndbeyond\.com\/sources\/.*?\//,
   );
   const baseUrl = findBaseUrl !== null ? findBaseUrl[0] : "";

   //Get the title of the adventure, in titleCase
   const title = titleCase(
      tocHTML.match(/(?<=<h1>)(?<title>.*?)(?=<\/h1>)/)[0].toLowerCase(),
   );

   //Construct the new ToC which goes into ToC journal
   let newToc = tocHTML
      .replace(/(?<=<h1>)(?<title>.*?)(?=<\/h1>)/, title) //Replace title with titleCase version
      .replace(/<ul>.*?<\/ul>/gs, ``) //Remove subcontent links/lists
      .replace(/<\/?(header|div|section|article)(\s.*?)?>/gm, ``) //Remove unnessecary tags
      .replace(
         /<a href=".*?\.(jpe?g|png)">.*?<\/a>/gm,
         `$&&nbsp;<em class="far fa-image">&nbsp;</em>`, //Add FA image icon to image links
      );

   // Create folders and initial JE for every chapter found
   let sections = tocHTML.matchAll(
      /(?<=<h3><a href=")(?<url>.*?)">(?<description>.*?)(?=<\/a>)/g,
   );

   // Parse each sections
   for (const s of sections) {
      let chapterUrl = s[1];
      let chapterName = s[2];

      //Don't make a folder or journal for a simple image link
      if (chapterUrl.match(/\.(jpe?g|png)$/i) !== null) continue;

      const sectionFolder = await Folder.create({
         name: chapterName,
         type: "JournalEntry",
         parent: ddbJiFolder.id,
         "flags.world.ddbhp-folder": true,
      });

      const sectionJournal = await JournalEntry.create(
         {
            name: chapterName,
            content: null,
            folder: sectionFolder.id,
            "flags.world.ddbhp": { status: "init", originalUrl: chapterUrl },
         },
         { renderSheet: false },
      );

      newToc = newToc.replace(
         /<a href="(?<url>.*?)">(?<description>.*?)<\/a>/g,
         function (wholeMatch, foundUrl, description) {
            if (foundUrl === chapterUrl)
               return `@JournalEntry[${sectionJournal.id}]{${description}}<a href="${chapterUrl}">&nbsp;&nbsp;<em class="fas fa-link">&nbsp;</em></a>`;

            return wholeMatch;
         },
      );
   }

   //Update ToC, names and baseUrl
   await ddbJiToc.update({
      name: "Table of Contents",
      content: newToc,
      flags: { ddbhp: { status: "OK", baseUrl: baseUrl } },
   });

   await ddbJiFolder.update({
      name: title,
   });

   /***************************************

   HELPER FUNCTIONS BELOW

 ****************************************/

   //Helper function to capitalize each word in a string
   function titleCase(str) {
      const regex = /(?!a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to)\b[a-z]\B/g;
      return str.replace(regex, match => match.toUpperCase());
   }

   //Remove some crap
   function cleanupHTML(html) {
      return html.replace(
         /( data-.*?=".*?"| class=".*?"| title=".*?"|<select.*?>.*?<\/select>)/gms,
         ``,
      );
   }

   //Replace SPAN containing font-* attributes with STRONG and EM tags, and then removes all style attributes
   function removeStyles(html) {
      const regex_SpanStyleCleanup = /<span style=".*?(font-weight: bold;)?.*?(font-style: italic;)?.*?">(?<content>.*?)<\/span>/g;
      const regex_RemoveStyles = / style=".*?"/g;
      const regex_htmlEntities = /(&nbsp;|&rsquo;)/g;

      return html
         .replace(
            regex_SpanStyleCleanup,
            function (wholeMatch, hasBold, hasItalics, content) {
               const isBold = wholeMatch.includes(`font-weight: bold;`);
               const isItalics = wholeMatch.includes(`font-style: italic;`);

               let result = ``;
               result += isBold ? "<strong>" : "";
               result += isItalics ? "<em>" : "";
               result += content;
               result += isBold ? "</strong>" : "";
               result += isItalics ? "</em>" : "";

               return result;
            },
         )
         .replace(regex_RemoveStyles, ``)
         .replace(regex_htmlEntities, match =>
            match === "&nbsp;" ? " " : "'",
         );
   }
})();

(async () => {
   const chapterJournal = game.journal.getName("Chapter 7: Hunting Lodge");
   const html = chapterJournal.data.content;
   const baseUrl = chapterJournal.getFlag("world", "ddbhp").originalUrl;

   let newHtml = html.replace(
      /<h3 id="(?<headingID>\w+)">(?<sectionName>[0-9]{1,2}[A-Z, ]*\.\s[\w\s']+)<\/h3>(?<sectionContent>.*?)(?=<h[123])/gs,
      function (wholeMatch, headingId, sectionName, sectionContent) {
         let matchUrl = `${baseUrl}#${headingId}`;
         let existingJournal = game.journal.filter(j => j.getFlag("world", "ddbhp")?.originalUrl === matchUrl);

         if(existingJournal.length === 0)
            return wholeMatch;
         
         return `<h3>@JournalEntry[${existingJournal[0].id}]{${sectionName}}</h3>`;
      }
   );
   await chapterJournal.update({content: newHtml});
})();

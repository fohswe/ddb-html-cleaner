(async () => {
   const parsedJournals = game.journal.filter(
      j => j.getFlag("world", "ddbhp")?.status === "parsed",
   );
   const urlRegex = /<a href="(?<url>.*?)">(?<description>.*?)<\/a>/g;

   parsedJournals.forEach(element => {
      console.log(`Parsing ${element.name}`);
      const html = element.data.content;
      let newHtml = html.replace(
         urlRegex,
         function (wholeMatch, url, description) {
            let existingJournal = game.journal.filter(j => j.getFlag("world", "ddbhp")?.originalUrl === url);
            if (existingJournal?.length !== 1)
               return wholeMatch;
            
            return `@JournalEntry[${existingJournal[0].id}]{${description}}`;
         },
      );
      updatecontent(newHtml, element);
   });

   async function updatecontent(newHtml, element) {
      await element.update({content: newHtml});
   }
})();

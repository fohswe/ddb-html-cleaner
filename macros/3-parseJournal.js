(async () => {
   const theJournals = game.journal.filter(j => j.getFlag("world","ddbhp")?.status === "init");
   let options = "";
   theJournals.forEach(j => options += `<option value="${j.id}">${j.name}</option>`)
   const content = `<label for="journal">Choose a journal:</label>
   <select name="journal" id="journal">
   ${options}
   </select>`;

   let d = new Dialog({
      title: "Parse Journal",
      content: content,
      buttons: {
         ok: {
            icon: '<i class="fas fa-check"></i>',
            label: "Ok",
            callback: (html) => {
               parseJournal(html.find('#journal').val());
            },
         },
         cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: "Cancel",
            callback: () => console.log("No journal parsed"),
         },
      },
      default: "cancel",
      render: (html) => ui.notifications.notify("Select a journal to parse"),
   });
   d.render(true);


/* ****************
HELPER FUNCTIONS BELOW
*******************/

async function parseJournal (journalId) {
   const journal = game.journal.get(journalId);
   const html = journal?.data.content;
   if (!html)
      return;
   
   const cleanedHTML = removeStyles(cleanupHTML(html));
   const linkedHTML = 
                     replaceOtherCommonLinks(
                        addUsingAbilityScoresLinks(
                           addConditionLinks(
                              addEntityLinks(
                                 addInlineRolls(cleanedHTML)
                              )
                           )
                        )
                     );
   await journal.update({"flags.world.ddbhp": { status: "parsed"}, content: linkedHTML});
}

//Remove some crap
function cleanupHTML(html) {
   return html.replace(
      /( data-.*?=".*?"| class=".*?"| title=".*?"|<select.*?>.*?<\/select>)/gms,
      ``
   );
}

//Replace SPAN containing font-* attributes with STRONG and EM tags, and then removes all style attributes
function removeStyles(html) {
   const regex_SpanStyleCleanup = /<span style=".*?(font-weight: bold;)?.*?(font-style: italic;)?.*?">(?<content>.*?)<\/span>/g;
   const regex_RemoveStyles = / style=".*?"/g;
   const regex_htmlEntities = /(&nbsp;|&rsquo;)/g;

   return html
      .replace(
         regex_SpanStyleCleanup,
         function (wholeMatch, hasBold, hasItalics, content) {
            const isBold = wholeMatch.includes(`font-weight: bold;`);
            const isItalics = wholeMatch.includes(`font-style: italic;`);

            let result = ``;
            result += isBold ? "<strong>" : "";
            result += isItalics ? "<em>" : "";
            result += content;
            result += isBold ? "</strong>" : "";
            result += isItalics ? "</em>" : "";

            return result;
         }
      )
      .replace(regex_RemoveStyles, ``)
      .replace(regex_htmlEntities, (match) => (match === "&nbsp;" ? " " : "'"));
}

// Create inline rolls for found dice formulas
function addInlineRolls(html) {
   return html.replace(
      /([0-9]{1,2})d([0-9]{1,3})(?: &ndash; )?([0-9]{0,3})/g,
      function (wholeMatch, numberOfDice, typeOfDice, modifier) {
         console.log(`Found the expression: ${wholeMatch}`);
         return Number.isInteger(modifier)
            ? `[[/r ${numberOfDice}d${typeOfDice} - ${modifier}]]`
            : `[[/r ${numberOfDice}d${typeOfDice}]]`;
      }
   );
}

// Create in world links for Actor and Item entities
function addEntityLinks(html) {
   return html.replace(
      /<a href="https:\/\/www\.dndbeyond\.com\/(monsters|equipment|magic-items|spells)\/(?<nameFromUrl>.*?)">(?<description>.*?)<\/a>/g,
      function (wholeMatch, type, nameFromUrl, description) {
         console.log(`Found ${nameFromUrl} with description ${description}`);
         let canonicalName = titleCase(description);

         //Try to decide if the monster name in the description is plural, by checking against the url name
         if (canonicalName.slice(-1) === "s" && nameFromUrl.slice(-1) !== "s")
            canonicalName = canonicalName.slice(0, -1);

         return type === "monsters"
            ? `@Actor[${titleCase(canonicalName)}]{${description}}`
            : `@Item[${titleCase(canonicalName)}]{${description}}`;
      }
   );
}

// Create links to conditions in the SRD Compendium
function addConditionLinks(html) {
   return html.replace(
      /<a href="https:\/\/www\.dndbeyond\.com\/basic-rules\/appendix-a-conditions#(?<condition>.*?)">(?<description>.*?)<\/a>/g,
      function (wholeMatch, condition, description) {
         let entityId = ``;

         switch (condition) {
            case "Blinded":
               entityId = "995mfaI3z7WtY7vK";
               break;
            case "Charmed":
               entityId = "H8aTFVG3YFAkQ3TK";
               break;
            case "Deafened":
               entityId = "XnDPIa0MLWhGvuQg";
               break;
            case "Frightened":
               entityId = "pAcHCDJBhybiVzMF";
               break;
            case "Grappled":
               entityId = "b4UvTt9P3As79cwm";
               break;
            case "Incapacitated":
               entityId = "tUYvSkhggFcMpVw5";
               break;
            case "Invisible":
               entityId = "hMWCeXenROGuhF1K";
               break;
            case "Paralyzed":
               entityId = "BJaVW22yVYe7dtzq";
               break;
            case "Petrified":
               entityId = "1XcKtGnKprqX6fa2";
               break;
            case "Poisoned":
               entityId = "prcOUhkWEVppFVbg";
               break;
            case "Prone":
               entityId = "ZkXz0qdkNBkKE6tN";
               break;
            case "Restrained":
               entityId = "QRKWz3p6v9Rl1Tzh";
               break;
            case "Stunned":
               entityId = "TiD5vnNql12Wh3Pn";
               break;
            case "Unconscious":
               entityId = "MOk1rkF0DeJlcC4l";
               break;
            case "Exhaustion":
               entityId = "apBZudO4UA1wj7aC";
               break;
         }

         if (!entityId) return;

         return `@Compendium[dnd5e.rules.${entityId}]{${description}}`;
      }
   );
}

// Create links to Using Ability Score in SRD Compendium
function addUsingAbilityScoresLinks(html) {
   return html.replace(
      /<a href="https:\/\/www\.dndbeyond\.com\/basic-rules\/using-ability-scores#(?<ability>.*?)">(?<description>.*?)<\/a>/g,
      function (wholeMatch, ability, description) {
         return `@Compendium[dnd5e.rules.tK5FwAIu92di2zLZ]{${description}}`;
      }
   );
}

// Match remaining URLs against common SRD rules etc
function replaceOtherCommonLinks(html) {
   const commonLinksURLs = [
      {
         url:
            "https://www.dndbeyond.com/compendium/rules/phb/adventuring#Suffocating",
         link: "@Compendium[dnd5e.rules.mKsmKfuLvJ3GjPzN]", //The Environment = Falling, Suffocating, Vision and Light, Food and Water, Interacting with Objects
      },
      //"" : "@Compendium[dnd5e.rules.oag7MR9Omacmhzb7]{Blindsight}",
      //"" : "@Compendium[dnd5e.rules.eeQ4uW3yMwnbIYVs]{Truesight}",
      //"" : "@Compendium[dnd5e.rules.MzNASNCgB0Qi2m8p]{Tremorsense}",
      //"" : "@Compendium[dnd5e.rules.10RNSKCH2hTja3DG]{Darkvision}",

      //"" : "@Compendium[dnd5e.rules.87bCRbO4gFLQJXX4]" //Movement = Speed, Special Types of Movement
      //"" : "@Compendium[dnd5e.rules.gRcWZ56MBxjX2mbO]" //Time
      //"" : "@Compendium[dnd5e.rules.gPaI6ycBI7MRz2IJ]" //Between Adventures = Lifestyle Expenses, Downtime Activities
      //"" : "@Compendium[dnd5e.rules.ei4esVo9XkUXRccq]" //Resting = Short Rest, Long Rest
   ];

   return html.replace(
      /<a href="(?<url>https:\/\/www\.dndbeyond\.com\/.*?)">(?<description>.*?)<\/a>/g,
      function (wholeMatch, url, description) {
         let result = commonLinksURLs.find((index) => index.url === url)?.link;

         if (result === undefined)
            return wholeMatch;

         return `${result}{${description}}`;
      }
   );
}

//Helper function to capitalize each word in a string
function titleCase(str) {
   const regex = /(?! a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per| the|to)(^[a-z]|\b[a-z]\B)/gm;
   return str.replace(regex, (match) => match.toUpperCase());
}

})();